import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';

@Component({
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {

  private _title: string;
  private _card_title: string = 'Hello World!';
  private _home_h2: string;
  private _list_itens: string[];
  private _list_cods: Array<number>;  
  private _first_name: string = 'Diego';
  private _last_name: string = 'Vinicius';
  private _full_name: string;
  private _text: string;

  constructor(private navCtrl: NavController) {
    this._title = "My Awesome App";
    this._home_h2 = "Welcome to my App";
    this._list_itens = ['item 1', 'item 2', 'item 3', 'item 4'];
    this._list_cods = [1, 2, 3];    
    //this._full_name = ${ this._first_name } ${ this._last_name };
    this._full_name = this._first_name + " " +this._last_name;    
    this._text = "Lorem ipsum dolor sit amet, ad sit morbi vulputate cursus fusce, aliquam nisl, maecenas proin cras felis magna vestibulum, vitae risus.";
  }

  public get title() : string{
    return this._title;
  }

  public set title(value : string){
    this._title = value;        
  }

}
