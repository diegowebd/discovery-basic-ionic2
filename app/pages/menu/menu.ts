import {Component} from '@angular/core';
import {App, MenuController} from 'ionic-angular';

@Component({
  templateUrl: 'build/pages/menu/menu.html'
})
export class MenuPage {
    constructor(app: App, public menuCtrl: MenuController) {
    menuCtrl.enable(true);
    }
    openMenu() {
        this.menuCtrl.open();
    }

    closeMenu() {
        this.menuCtrl.close();
    }

    toggleMenu() {
        this.menuCtrl.toggle();
    }
  
}

@Component({
  templateUrl: './build/pages/menu/menu-places.html'
})
export class PageOne { }

@Component({
  templateUrl: './build/pages/menu/menu-friends.html'
})
export class PageTwo { }

@Component({
  templateUrl: './build/pages/menu/menu-events.html'
})
export class PageThree { }